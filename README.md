# Koboldworks – Starter Bag for Pathfinder 1e

Allows defining starting items for all new actors.

It's pretty great thing to have. Not necessary, but simplifies character setup.

  ✅ Recommended for general use

## Rationale

- Homebrew, houserules, campaign specifics, etc. that necessitate existence of new items (misc features, buffs, equipment, etc.) to be added to every single actor. This is unnecessary busywork for players and GMs alike.

## Known issues

- The starter bag allows including races but does not limit their number like the character sheet. This may cause undesirable side effects. There should be no reason to have default race item for actors anyway, unless your campaign is constraining everyone to the same race.

## Limitations

- References from actors or tokens are not allowed. Technically this is possible, but for everyone's sanity these are blocked.

## API

```js
const sb = game.modules.get('koboldworks-pf1-starter-bag').api;
for (let key of Object.keys(sb.bags)) {
	console.log(sb.bags[key]) // => [{ id: itemId, name: displayName, source: compendiumID}]
}
sb.getItem(bagEntry); // Gathers extended item data for an .bags entry.
sb.getBag(bagId); // similar to .bags, but gets singular bag with items already resolved via .getItem()
sb.update(); // Inverse of compress, adds or updates non-critical auxillary data.
sb.test(); // Basic testing of bag contents. Output in console.
sb.migrate(); // Attempts to refresh and migrate the internal data
```

## Install

Manifest URL: <https://gitlab.com/koboldworks/pf1/starter-bag/-/releases/permalink/latest/downloads/module.json>

### Older Versions

Foundry v9 compatible manifest: <https://gitlab.com/koboldworks/pf1/starter-bag/-/raw/0.2.0.3/module.json>  
... Manual install: <https://gitlab.com/koboldworks/pf1/starter-bag/-/archive/0.2.0.3/starter-bag-0.2.0.3.zip>

## Attribution

If you use any of the code in this project, I would appreciate I or the project was credited for inspiration or whatever where appropriate. Or just drop a line about using my code. I do not mind not having this, but it's just nice knowing something has come out of my efforts.

## Donations

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/I2I13O9VZ)

## License

This software is distributed under the [MIT License](./LICENSE).
