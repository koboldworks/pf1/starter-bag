import { CFG } from './config.mjs';
import { ItemReference } from './item-reference.mjs';

class BagEntry extends foundry.abstract.DataModel {
	static defineSchema() {
		const fields = foundry.data.fields;
		return {
			id: new fields.StringField({ required: false, nullable: true, blank: false, initial: null }),
			source: new fields.StringField({ required: false, nullable: true, blank: false, initial: null }),
			uuid: new fields.StringField(),
			name: new fields.StringField({ required: false, nullable: true, blank: false, initial: null }),
		};
	}

	get hasOldSource() {
		return !!this.source && !!this.id;
	}

	/**
	 * @returns {Promise<Item|undefined>}
	 */
	async getOldSource() {
		const pack = game.packs.get(this.source);
		return pack?.getDocument(this.id);
	}
}

/**
 * @property {BagEntry[]} all
 * @property {BagEntry[]} npc
 * @property {BagEntry[]} pc
 */
export class StarterBag extends foundry.abstract.DataModel {
	static defineSchema() {
		const fields = foundry.data.fields;
		return {
			all: new fields.ArrayField(new fields.EmbeddedDataField(BagEntry), { initial: [] }),
			npc: new fields.ArrayField(new fields.EmbeddedDataField(BagEntry), { initial: [] }),
			pc: new fields.ArrayField(new fields.EmbeddedDataField(BagEntry), { initial: [] }),
		}
	}

	static async getItem(itemData, { verbose = true } = {}) {
		const iRef = new ItemReference(itemData);

		let item;
		if (!iRef.uuid) {
			if (iRef.source) {
				// Compendium
				const pack = game.packs.get(iRef.source);
				if (!pack) {
					if (verbose) ui.notifications.error(game.i18n.format('StarterBag.Notify.PackNotFound', { ref: iRef.source }));
				}
				else {
					item = await pack.getDocument(iRef.id);
					iRef.source = pack.collection;
					iRef.sourceLabel = pack.metadata.label;
					if (!item) iRef.state.missing = true;
				}
			}
			else if (iRef.id) {
				// Items Directory
				item = game.items.get(iRef.id);
				iRef.sourceLabel = game.i18n.localize('DOCUMENT.Items');
				if (item) {
					iRef.state.directory = true;
					iRef.uuid = item.uuid;
				}
				else {
					iRef.state.missing = true;
				}
			}
		}

		if (iRef.uuid && !item) {
			item = await fromUuid(iRef.uuid);
			if (!item) {
				if (verbose) ui.notifications.error(game.i18n.format('StarterBag.Notify.ItemNotFound', { ref: iRef.uuid }));
				iRef.sourceLabel = '<MISSING>';
				iRef.type = '<MISSING>';
				iRef.name ??= '<MISSING>';
			}
			else {
				iRef.name ??= item.name;
				if (item.pack) {
					const pack = game.packs.get(item.pack);
					iRef.source = pack.collection;
					iRef.sourceLabel = pack.metadata.label;
				}
				else {
					iRef.source = null;
					iRef.sourceLabel = game.i18n.localize('DOCUMENT.Items');
					if (item) iRef.state.directory = true;
				}
			}
		}
		// Backwards compatibility
		else if (iRef.source) {
			// Compendium
			const pack = game.packs.get(iRef.source);
			if (!pack) {
				if (verbose) ui.notifications.error(game.i18n.format('StarterBag.Notify.PackNotFound', { ref: iRef.source }));
			}
			else {
				// if (!pack.indexed) await pack.getIndex();
				item = await pack.getDocument(iRef.id);
				iRef.source = pack.collection;
				iRef.sourceLabel = pack.metadata.label;
			}
		}
		else if (iRef.id) {
			// Items Directory
			item = game.items.get(iRef.id);
			iRef.source = null;
			iRef.sourceLabel = game.i18n.localize('DOCUMENT.Items');
			if (item) iRef.state.directory = true;
		}

		iRef.document = item;

		iRef.type ??= '<MISSING>';
		iRef.name ??= '<MISSING>';
		iRef.sourceLabel ??= '<MISSING>';
		iRef.uuid ??= `missing-no-${randomID()}`;

		if (!item)
			iRef.state.missing = true;
		else {
			iRef.name = item.name;
			if (item.img) iRef.img = item.img;
			else {
				iRef.img = Item.DEFAULT_ICON;
				iRef.state.image = false;
			}
			// TODO: Use translated strings for types
			iRef.type = item.type;
			iRef.typeLabel = game.i18n.localize(CONFIG.PF1.itemTypes[item.type]);
			if (['weapon', 'race'].includes(item.type)) iRef.subType = undefined; // Not useful subtyping logic for us
			else iRef.subType = item.subType ?? item.system[`${item.type}Type`]; // TODO: Improve subtyping

			if (iRef.subType) iRef.subTypeLabel = CONFIG.PF1[`${item.type}Types`][iRef.subType];
		}
		return iRef;
	}

	/**
	 * @type {StarterBag}
	 */
	static get bags() {
		return game.settings.get(CFG.id, CFG.SETTINGS.config);
	}

	/**
	 * Refreshes and refills the internal data.
	 */
	static async migrateBag() {
		const config = this.bags.toObject();

		let updated = 0;
		for (const key of Object.keys(config)) {
			for (const itemData of config[key]) {
				const iRef = await StarterBag.getItem(itemData, { verbose: false });
				if (!iRef.document) continue; // bad item
				if (itemData.name !== iRef.document.name) {
					itemData.name = iRef.document.name;
					updated++;
				}
				if (!itemData.uuid) {
					itemData.uuid = iRef.document.uuid;
					delete itemData.source;
					updated++;
				}
				else if (itemData.source) {
					delete itemData.source;
					updated++;
				}
			}
		}

		if (updated > 0) game.settings.set(CFG.id, CFG.SETTINGS.config, config);
		return updated;
	}

	/**
	 * @param {"all"|"pc"|"npc"} bagId
	 * @returns
	 */
	static async getBag(bagId) {
		const actual = [];
		const bags = this.bags;
		const bag = bags[bagId];
		if (!bag) return [];

		const p = bag.map(iData => StarterBag.getItem(iData).then(item => {
			if (!item) ui.notifications.warn(game.i18n.format('StarterBag.Notify.MissingItem', { bag: bagId, ref: iData.id }));
			return item ?? null;
		}));

		actual.push(...(await Promise.all(p)).filter(i => i != null));

		return actual;
	}

	static async test() {
		const bags = game.settings.get(CFG.id, CFG.SETTINGS.config);
		let broken = 0;

		for (const key of Object.keys(bags))
			bags[key] = await StarterBag.getBag(key);

		console.group(CFG.label);
		try {
			for (const key of Object.keys(bags)) {
				const bag = bags[key];
				console.group(`Bag: ${key}`);
				let brokenInBag = 0;
				try {
					for (const item of bag) {
						if (!item.document) {
							broken++;
							brokenInBag++;
							console.warn(`Missing [${item.id}]: ${item.name ?? '<n/a>'} [${item.source ?? '<Directory>'}]`);
						}
					}
					if (brokenInBag == 0)
						console.log('No errors.');
				}
				finally {
					console.groupEnd();
				}
			}
			if (broken == 0)
				console.log('No errors found.');
			else
				console.log('Errors:', broken);
		}
		finally {
			console.groupEnd();
		}
	}
}
