import { CFG } from './config.mjs';
import './settings.mjs';
import './create-handler.mjs';
import './pre-create-handler.mjs';
import { StarterBag } from './bag.mjs';
import { StarterBagForm } from './bag-form.mjs';

// Setup API
Hooks.once('setup', () => {
	const api = {
		get bags() { return StarterBag.bags; },
		getItem: StarterBag.getItem,
		getBag: StarterBag.getBag,
		test: StarterBag.test,
		migrate: StarterBag.migrateBag,
		setup: () => {
			const app = new StarterBagForm();
			app.render(true, { focus: true });
			return app;
		},
	};

	game.modules.get(CFG.id).api = api;
});

// Perform migration
Hooks.once('ready', () => {
	if (game.user.isGM)
		StarterBag.migrateBag();
});
