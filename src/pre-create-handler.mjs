import { CFG } from './config.mjs';

/**
 * @param {Actor} actor
 * @param {Object} data
 * @param {Object} options
 */
const preCreateActorEvent = (actor, data, options) => {
	const handled = actor.getFlag(CFG.id, 'handled');
	if (handled === true) return;

	options.__starterBag = { addItems: true };
	actor.updateSource(
		{ flags: { [CFG.id]: { handled: true } } },
		{ render: false }
	);
}

let precreatehook;
export const toggleFlagging = (enable = true) => {
	if (enable && !precreatehook) precreatehook = Hooks.on('preCreateActor', preCreateActorEvent);
	else if (precreatehook) Hooks.off('preCreateActor', precreatehook);
}

Hooks.once('setup', () => toggleFlagging());
