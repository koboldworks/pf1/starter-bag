import { CFG } from './config.mjs';
import { StarterBagForm } from './bag-form.mjs';
import { StarterBag } from './bag.mjs';

import { toggleFlagging } from './pre-create-handler.mjs';

Hooks.once('init', () => {
	game.settings.register(
		CFG.id,
		CFG.SETTINGS.config,
		{
			type: StarterBag,
			default: new StarterBag(),
			scope: 'world',
			config: false,
		}
	);

	game.settings.registerMenu(
		CFG.id, CFG.SETTINGS.config,
		{
			name: 'StarterBag.Label',
			hint: 'StarterBag.Hint',
			label: 'StarterBag.Title',
			id: `${CFG.id}-config`,
			icon: 'fas fa-suitcase',
			type: StarterBagForm,
			restricted: true,
		}
	);

	game.settings.register(
		CFG.id,
		CFG.SETTINGS.dupeFilter,
		{
			name: 'StarterBag.Settings.Duplicates.Name',
			hint: 'StarterBag.Settings.Duplicates.Hint',
			type: Number,
			default: 1,
			choices: {
				0: 'StarterBag.Settings.Duplicates.Empty',
				1: 'StarterBag.Settings.Duplicates.Source',
				2: 'StarterBag.Settings.Duplicates.Flag',
			},
			scope: 'world',
			config: true,
			onChange: value => {
				if (value === 2) toggleFlagging(true);
				else toggleFlagging(false);
			}
		}

	)
});
