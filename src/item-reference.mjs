export class ItemReference {
	id;
	uuid;
	name;
	source;
	sourceLabel;
	typeof;
	subType;
	img;
	state = { missing: false, image: true, directory: false };
	document;

	constructor(data) {
		this.id = data.id;
		this.uuid = data.uuid;
		this.name = data.name;
		this.source = data.source;
	}
}
