import { CFG } from './config.mjs';
import { StarterBag } from './bag.mjs';

/** Map actor.type to bag */
const actorTypeMap = {
	character: 'pc',
	npc: 'npc'
};

/**
 * @param {Actor} actor
 * @param {Object} options
 * @param {String} userId
 */
const createActorEvent = async (actor, options, userId) => {
	if (options.temporary) return;
	if (game.user.id !== userId) return; // user does not match creator

	const type = actorTypeMap[actor.type];

	if (!type)
		return void ui.notifications.warn(game.i18n.format('StarterBag.Notify.UnknownActor', { id: actor.id }));

	const filter = game.settings.get(CFG.id, CFG.SETTINGS.dupeFilter);

	// Ignore actors with more than 0 items.
	if (filter === 0 && actor.items.contents.length > 0) return;
	// Ignore actors that don't need items added
	if (filter === 2 && !options.__starterBag?.addItems) return;

	const itemsForAll = await StarterBag.getBag('all');
	const itemsForType = await StarterBag.getBag(type);

	const actualItems = [...itemsForAll.map(i => i.document), ...itemsForType.map(i => i.document)]
		.filter((item, index, arr) => !!item && arr.findIndex(i => i.uuid == item.uuid) == index);

	if (actualItems.length == 0) return; // No items to add

	let createData = actualItems.map(i => i.toObject());

	if (filter === 1) {
		// Filter data based on name, type and source ID matching. Slow but fairly reliable.
		const itemTypes = actor.itemTypes;
		createData = createData.filter(i => {
			const sameTyped = itemTypes[i.type];
			if (!sameTyped) return true; // No items of same type
			const sourceId = i.flags?.core?.sourceId;
			const same = sameTyped
				.filter(o => o.name === i.name) // Same name
				.filter(o => o.flags?.core?.sourceId === sourceId); // Same source
			return same.length == 0;
		});
	}

	if (createData.length == 0) return;

	// Fix spells so they have valid spellbook
	const books = new Set();
	for (const item of createData) {
		if (item.type === 'spell') {
			// Add spellbook if missing
			item.system.spellbook ||= 'primary';
			books.add(item.system.spellbook);
		}
	}

	await actor.createEmbeddedDocuments('Item', createData);

	// Auto-enable books that were encountered
	if (books.size) {
		let doUpdate = false;
		const updateData = { system: {} };
		for (const bookId of books) {
			const bookPath = `attributes.spells.spellbooks.${bookId}`;
			const bookData = getProperty(actor.system, bookPath);
			if (bookData && bookData.inUse !== true) {
				doUpdate = true;
				setProperty(updateData.system, bookPath, { inUse: true });
			}
		}

		if (doUpdate)
			await actor.update(updateData);
	}
}

Hooks.on('createActor', createActorEvent);
