import { CFG } from './config.mjs';
import { StarterBag } from './bag.mjs';

const template = `modules/${CFG.id}/template/form.hbs`;
Hooks.once('setup', () => loadTemplates([`modules/${CFG.id}/template/form-listing.hbs`]));

const bagLabels = {
	all: 'StarterBag.Category.All',
	pc: 'StarterBag.Category.PC',
	npc: 'StarterBag.Category.NPC',
}

export class StarterBagForm extends FormApplication {
	/** @type {StarterBag} */
	bags;

	constructor() {
		super(...arguments);

		Object.entries(bagLabels)
			.forEach(([key, label]) => bagLabels[key] = game.i18n.localize(label));

		this.bags = StarterBag.bags;
	}

	get template() {
		return `modules/${CFG.id}/template/form.hbs`;
	}

	/** @override */
	static get defaultOptions() {
		const options = super.defaultOptions;
		return {
			...options,
			id: `${CFG.id}-config`,
			title: 'StarterBag.Title',
			classes: [...options.classes, 'koboldworks', 'starter-bag'],
			dragDrop: [{ dragSelector: null /* '.item' */, dropSelector: '.content' }],
			tabs: [{ navSelector: '.tabs', contentSelector: '.content', initial: 'all' }],
			scrollY: ['ol.item-list'],
			width: 520,
			height: 380,
			closeOnSubmit: true,
			resizable: true
		};
	}

	/** @override */
	async getData(options = {}) {
		const context = super.getData(options);
		context.bags = {
			all: [],
			pc: [],
			npc: [],
		};

		const groups = Object.keys(this.bags);

		const inOtherGroups = (id, key) => groups
			.filter(g => g !== 'all' && g !== key)
			.filter(k => this.bags[k].find(i => i.id === id));

		const p = [];
		for (const key of groups) {
			for (const i of this.bags[key]) {
				const pi = StarterBag.getItem(i);
				pi.then(item => context.bags[key].push(item));
				p.push(pi);
			}
		}

		for (const [key, data] of Object.entries(context.bags)) {
			data._label = bagLabels[key];
		}

		await Promise.allSettled(p);

		// TODO: Check same ID in multiple groups
		// if (key !== 'all' && inOtherGroups(i.id, key).length)
		// console.warn(`STARTER BAG | Found ID [${i.id}] in 'all' and one or more subgroups.`);

		console.log(context);

		return context;
	}

	async _onRefreshItems(event) {
		event.preventDefault();
		event.stopPropagation();
		const updated = await StarterBag.migrateBag();
		ui.notifications.info(game.i18n.format('StarterBag.Notify.RefreshUpdate', { count: updated }));
		this.render();
	}

	_onClearItems(event) {
		event.preventDefault();
		event.stopPropagation();
		this.bags = new StarterBag();
		this.render();
	}

	async _deleteItem(event) {
		event.stopPropagation();
		event.preventDefault();
		const iel = event.target.closest('.item');
		const tab = event.target.closest('.tab.active[data-group="sections"]').dataset.tab;
		const uuid = iel.dataset.uuid;

		const data = this.bags.toObject();
		data[tab] = data[tab].filter(i => i.uuid !== uuid);
		this.bags.updateSource({ [tab]: data[tab] });

		this.render();
	}

	async _openItem(event) {
		event.stopPropagation();
		event.preventDefault();
		const iel = event.target.closest('.item');
		const item = await StarterBag.getItem({ uuid: iel.dataset.uuid });
		item?.document?.sheet.render(true);
	}

	/**
	 * @param {JQuery} jq
	 * @override
	 */
	activateListeners(jq) {
		super.activateListeners(jq);
		const html = jq[0];
		const buttons = html.querySelector('footer');
		buttons.querySelector('button[name=clear]')
			.addEventListener('click', this._onClearItems.bind(this));
		buttons.querySelector('button[name=refresh]')
			.addEventListener('click', this._onRefreshItems.bind(this));

		html.querySelectorAll('.item .controls [data-action]').forEach(el => {
			const action = el.dataset.action;
			if (action === 'delete')
				el.addEventListener('click', this._deleteItem.bind(this));
			else if (action === 'edit')
				el.addEventListener('click', this._openItem.bind(this));
		});
	}

	/** @override */
	async _updateObject(_, _form) {
		game.settings.set(CFG.id, CFG.SETTINGS.config, this.bags.toObject());
	}

	/** @override */
	async _onDrop(event) {
		let data;
		try {
			data = JSON.parse(event.dataTransfer.getData('text/plain'));
		}
		catch (err) {
			return void ui.notifications.error('Drop data error.');
		}

		if (data.type !== 'Item')
			return void ui.notifications.warn(game.i18n.localize('StarterBag.Notify.NonItemDrop'));

		const tabSelector = '.tab.active[data-group="sections"]';
		const tab = (event.target.closest(tabSelector) ?? event.target.querySelector(tabSelector)).dataset.tab;
		if (!tab) return void ui.notifications.warn('Unexpected drop location, can\'t find data.');

		const item = await fromUuid(data.uuid);

		if (item.actor) return void ui.notifications.warn(game.i18n.localize('StarterBag.Notify.NoActorRef'));

		const iData = { id: item.id, source: item.pack, uuid: data.uuid, name: item.name };

		if (this.bags[tab].find(i => i.uuid === data.uuid))
			return void ui.notifications.warn(game.i18n.localize('StarterBag.Notify.Duplicate'));
		else if (tab !== 'all' && this.bags.all.find(i => i.uuid === data.uuid))
			return void ui.notifications.warn(game.i18n.format('StarterBag.Notify.DuplicateInBag', { bag: bagLabels.all }));

		const list = this.bags.toObject()[tab];
		list.push(iData);

		this.bags.updateSource({ [tab]: list })

		this.render();
	}
}
