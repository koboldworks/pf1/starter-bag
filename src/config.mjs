export const CFG = {
	id: 'koboldworks-pf1-starter-bag',
	label: 'Starter Bag 👜',
	SETTINGS: {
		config: 'bag',
		dupeFilter: 'filter',
	}
};
