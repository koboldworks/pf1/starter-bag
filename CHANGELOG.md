# Change Log

## NEXT

- i18n: `Koboldworks.` prefix removed from translation keys.
- API: Added `setup()` function that opens the configuration dialog.
- Change: Bag data is now validated by DataModel

## 1.1.0

- Change: Bag configuration UI overhauled to _Be Better_.
- New: Adding spells now automatically fills in primary spellbook if missing.
- New: Adding spells automatically enables spellbooks the spells reference.

## 1.0.0.1

- Fix: isGM error at launch that prevented automatic setting migration.

## 1.0.0

- Foundry v10 compatibility
- Support for v9 and older dropped

## 0.2.0.3

- Fixed actor duplication guard being over-zealous.
  - Duplication guard defaults to testing if type, name, and flags.core.sourceId match to get soft guarantee on same item existing.
  - Previously used method is still available in module config.
  - Third method is also available. It is fast, but fails easily.

## 0.2.0.2

- Fixed actor duplication causing the actors to get their starter bag items again.

## 0.2.0.1

- Fixed actor creation code running on all users.

## 0.2.0

- Fixed and functional
- Foundry 0.7 compatibility dropped.

## 0.1.1 Foundry 0.7.x cross-compatibility

- Fixed: Spurious claims of duplicate IDs in console log.

## 0.1.0 Initial release
